/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.views;

import application.other.IconApply;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class DlgStudentCourses extends JDialog {

    String studentID;
    Collection<String> listCourses;

    JPanel jPanelTop;
    JPanel jPanelBottom;

    JLabel jLabelNewCourse;
    JTextField jTextFieldNewCourse;
    JButton jButtonAddCourse;
    JButton jButtonPasteCourse;

    JLabel jLabelStudentID;
    JScrollPane jScrollPaneCourses;
    JList<Object> jListCourses;
    JButton jButtonRemoveCourses;

    JButton jButtonClose;

    public DlgStudentCourses(JFrame parent, Collection<String> courses, String studentID) {
        super(parent, true);
        setTitle("Student Courses");
        this.listCourses = courses;
        this.studentID = studentID;

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        initLayout();
        loadLayout();
        attachHandlers();

        setSize(new Dimension(400, 400));
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private void initLayout() {
        jPanelTop = new JPanel(new GridBagLayout());

        jPanelBottom = new JPanel();
        jPanelBottom.setLayout(new BoxLayout(jPanelBottom, BoxLayout.LINE_AXIS));

        jLabelNewCourse = new JLabel("New Course");
        jTextFieldNewCourse = new JTextField();
        jButtonAddCourse = new JButton("Add Course");
        jButtonPasteCourse = new JButton("Paste Course");
        jButtonPasteCourse.setEnabled(false);

        jLabelStudentID = new JLabel("Student ID: " + this.studentID);
        jListCourses = new JList<>(listCourses.toArray());
        jListCourses.setListData(listCourses.toArray());
        jListCourses.setCellRenderer(new CustomCellRenderer());
        jScrollPaneCourses = new JScrollPane(jListCourses);
        jButtonRemoveCourses = new JButton("Remove Courses");
        jButtonRemoveCourses.setEnabled(false);

        jButtonClose = new JButton("Close");
    }

    private void loadLayout() {
        //gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
        //top left bottom right
        jPanelTop.add(jLabelNewCourse, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
        jPanelTop.add(jTextFieldNewCourse, new GridBagConstraints(1, 0, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 2, 0, 10), 0, 0));

        JPanel jPanelTopButtons = new JPanel();
        jPanelTopButtons.add(jButtonPasteCourse);
        jPanelTopButtons.add(jButtonAddCourse);
        jPanelTop.add(jPanelTopButtons, new GridBagConstraints(0, 1, 2, 1, 0.0d, 0.0d,
                GridBagConstraints.LAST_LINE_END, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));

//        jPanelTop.add(jButtonAddCourse, new GridBagConstraints(1, 1, 1, 1, 0.0d, 0.0d,
//                GridBagConstraints.LAST_LINE_END, GridBagConstraints.NONE, new Insets(0, 0, 0, 10), 0, 0));
        jPanelTop.add(jLabelStudentID, new GridBagConstraints(0, 2, 2, 1, 1.0d, 0.0d,
                GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 20, 0, 20), 0, 0));
        jPanelTop.add(jScrollPaneCourses, new GridBagConstraints(0, 3, 2, 1, 1.0d, 1.0d,
                GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 20, 0, 20), 0, 0));
        jPanelTop.add(jButtonRemoveCourses, new GridBagConstraints(1, 4, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LAST_LINE_END, GridBagConstraints.NONE, new Insets(0, 20, 10, 20), 0, 0));

        jPanelBottom.add(Box.createHorizontalGlue());
        jPanelBottom.add(jButtonClose);
        jPanelBottom.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        getContentPane().add(jPanelTop, BorderLayout.CENTER);
        getContentPane().add(jPanelBottom, BorderLayout.SOUTH);
    }

    private void attachHandlers() {
        jButtonPasteCourse.addActionListener((event) -> {
            if (jListCourses.getSelectedValue() == null) {
                JOptionPane.showMessageDialog(this, "Select a course to paste into textfield above.");
            } else {
                jTextFieldNewCourse.setText(jListCourses.getSelectedValue().toString());
            }
        });

        jButtonAddCourse.addActionListener((event) -> {
            String newCourse = jTextFieldNewCourse.getText().trim();

            if (newCourse.isEmpty()) {
                JOptionPane.showMessageDialog(this, "You should enter the course name.");
            } else if (listCourses.contains(newCourse)) {
                JOptionPane.showMessageDialog(this, "The course is already in the list.");
            } else {
                listCourses.add(newCourse);
                updateList();
            }
        });

        jButtonRemoveCourses.addActionListener((event) -> {
            List<Object> selectedCourses = jListCourses.getSelectedValuesList();
            if (selectedCourses.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Select courses first.");
            } else {

                int returnVal = JOptionPane.showOptionDialog(this, "Do you want to remove highlighted courses from the list?",
                        "Delete Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

                if (returnVal == JOptionPane.YES_OPTION) {
                    for (Object course : selectedCourses) {
                        listCourses.remove(course);
                    }
                    updateList();
                }
            }
        });

        jButtonClose.addActionListener((event) -> {
            this.setVisible(false);
        });

        jListCourses.addListSelectionListener((event) -> {
            jButtonPasteCourse.setEnabled(jListCourses.getSelectedValue() != null);
            jButtonRemoveCourses.setEnabled(jListCourses.getSelectedValue() != null);
        });
    }

    private void updateList() {
        jListCourses.setListData(listCourses.toArray());
    }

    class CustomCellRenderer extends JLabel implements ListCellRenderer {

        Color highlightColor;

        public CustomCellRenderer() {
            setOpaque(true);
            highlightColor = new Color(20, 20, 150);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String course = value.toString();
            this.setText(course);
            if (isSelected) {
                setBackground(highlightColor);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            return this;
        }
    }
}
