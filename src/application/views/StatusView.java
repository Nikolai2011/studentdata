/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.views;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class StatusView extends JPanel {

    private JLabel labelFilePath;

    /**
     * @author Nikolai Kolbenev 15897074
     */
    public StatusView() {
        this.setBorder(new BevelBorder(BevelBorder.LOWERED));

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        labelFilePath = new JLabel();
        labelFilePath.setHorizontalAlignment(SwingConstants.LEFT);

        this.add(labelFilePath);
    }

    /**
     * @author Nikolai Kolbenev 15897074
     */
    public JLabel getFilePathLabel() {
        return labelFilePath;
    }
}
