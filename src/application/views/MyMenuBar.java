/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.views;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MyMenuBar extends JMenuBar {

    JMenu menuFile;
    JMenuItem menuItemSave;
    JMenuItem menuItemSaveAs;
    JMenuItem menuItemLoadSet;
    

    JMenu menuStudent;
    JMenuItem menuItemAddStudent;
    JMenuItem menuItemRemoveStudent;
    JMenuItem menuItemNewSet;
    
    JMenu menuView;
    JMenuItem menuItemRainbow;
    JMenuItem menuItemTwoColorCyan;
    JMenuItem menuItemTwoColorRed;
    JMenuItem menuItemDefault;

    public MyMenuBar() {
        initComponents();
        loadComponents();
    }

    private void initComponents() {
        menuFile = new JMenu("File");
        menuItemNewSet = new JMenuItem("New set");
        menuItemLoadSet = new JMenuItem("Load set...");
        menuItemSave = new JMenuItem("Save data");
        menuItemSaveAs = new JMenuItem("Save data as...");
        
        menuStudent = new JMenu("Student");
        menuItemAddStudent = new JMenuItem("Add new student...");
        menuItemRemoveStudent = new JMenuItem("Remove student...");
        
        menuView = new JMenu("Photo View");
        menuItemRainbow = new JMenuItem("Rainbow");
        menuItemTwoColorRed = new JMenuItem("Two color red");
        menuItemTwoColorCyan = new JMenuItem("Two color cyan");
        menuItemDefault = new JMenuItem("Default");
    }

    private void loadComponents() {
        menuFile.add(menuItemNewSet);
        menuFile.addSeparator();
        menuFile.add(menuItemLoadSet);
        menuFile.addSeparator();
        menuFile.add(menuItemSave);
        menuFile.add(menuItemSaveAs);
        
        menuStudent.add(menuItemAddStudent);
        menuStudent.add(menuItemRemoveStudent);
        
        menuView.add(menuItemRainbow);
        menuView.add(menuItemTwoColorRed);
        menuView.add(menuItemTwoColorCyan);
        menuView.add(menuItemDefault);

        this.add(menuFile);
        this.add(menuStudent);
        this.add(menuView);
    }
}
