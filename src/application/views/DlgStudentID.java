/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class DlgStudentID extends JDialog {

    Collection<String> listIDs;

    JPanel jPanelTop;
    JPanel jPanelBottom;

    JLabel jLabelID;
    JLabel jLabelFeedback;
    JTextField jTextFieldID;
    JButton jButtonCreate;

    String selectedID;
    boolean isAddDialog;

    public DlgStudentID(JFrame parent, Collection<String> IDs, boolean isAddDialog) {
        super(parent, true);
        setTitle("Student ID");
        this.listIDs = IDs;
        this.isAddDialog = isAddDialog;

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        initLayout();
        loadLayout();
        attachHandlers();

        setSize(new Dimension(250, 150));

        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private void initLayout() {
        jPanelTop = new JPanel(new GridBagLayout());

        jPanelBottom = new JPanel();
        jPanelBottom.setLayout(new BoxLayout(jPanelBottom, BoxLayout.LINE_AXIS));

        jLabelFeedback = new JLabel(" ");
        jLabelID = new JLabel("ID: ");
        jTextFieldID = new JTextField();

        jButtonCreate = new JButton((isAddDialog) ? "Create Student" : "Remove Student");
    }

    private void loadLayout() {
        //gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
        jPanelTop.add(jLabelID, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 10, 0, 0), 0, 0));
        jPanelTop.add(jTextFieldID, new GridBagConstraints(1, 0, 1, 1, 1.0d, 0.0d, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 2, 0, 10), 0, 0));
        jPanelTop.add(jLabelFeedback, new GridBagConstraints(0, 1, 2, 1, 1.0d, 1.0d, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.NONE, new Insets(10, 10, 0, 10), 0, 0));

        jPanelBottom.add(Box.createHorizontalGlue());
        jPanelBottom.add(jButtonCreate);
        jPanelBottom.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        getContentPane().add(jPanelTop, BorderLayout.CENTER);
        getContentPane().add(jPanelBottom, BorderLayout.SOUTH);
    }

    private void attachHandlers() {
        jButtonCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Integer id = Integer.valueOf(jTextFieldID.getText().trim());
                    if (isAddDialog) {
                        if (!listIDs.contains(id.toString())) {
                            selectedID = id.toString();
                            setVisible(false);
//                        dispose();
                        } else {
                            setFeedback("That ID already exists.");
                        }
                    } else {
                        //Alt+Shift+F will merge this with else???
                        if (listIDs.contains(id.toString())) {
                            selectedID = id.toString();
                            setVisible(false);
//                        dispose();
                        } else {
                            setFeedback("The specified ID doesn't exist.");
                        }
                    }

                } catch (NumberFormatException ex) {
                    setFeedback("Value must be an integer.");
                } catch (Exception ex) {
                    setFeedback("Exception: " + ex.toString());
                }
            }
        });
    }

    private void setFeedback(String feedback) {
        if (jLabelFeedback.getText().equalsIgnoreCase(feedback)) {
            Font boldFont = new Font(jLabelFeedback.getFont().getFontName(), Font.BOLD, jLabelFeedback.getFont().getSize());
            jLabelFeedback.setFont(boldFont);
        } else {
            JLabel jLabel = new JLabel();//Sorry, don't want my java program to crash
            Font plainFont = new Font(jLabel.getFont().getFontName(), Font.PLAIN, jLabelFeedback.getFont().getSize());
            jLabelFeedback.setFont(plainFont);
        }

        jLabelFeedback.setText(feedback);
    }

    public String getSelectedID() {
        return selectedID;
    }
}
