/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.views;

import application.model.StudentInfo;
import application.model.StudentInfoSet;
import application.other.Constants;
import application.other.FileManipulation;
import application.other.Notifiable;
import application.other.Reusable;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.StrokeBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

public class StudentDisplayGUI extends JFrame {

    StudentDisplayGUI self = this;
    MyMenuBar menuBar;
    StudentInfoSet studentInfoSet;
    String pathToXmlData;
    BiFunction<Integer, Integer, Border> supplierBorder;

    AppView appView;

    public StudentDisplayGUI(String title) {
        super(title);

        setLayout(new BorderLayout());
        setSize(850, 650);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        studentInfoSet = new StudentInfoSet();
        supplierBorder = (width, height) -> {
            return Reusable.getTwoColorGradientRed(width, height);
        };
        ((JPanel)getContentPane()).setDoubleBuffered(true);
        initLayout();
        loadLayout();
        attachHandlers();
    }

    private void initLayout() {
        menuBar = new MyMenuBar();
        appView = new AppView();
    }

    private void loadLayout() {
        setJMenuBar(menuBar);
        getContentPane().add(appView);
    }

    private void attachHandlers() {
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (suggestSavingAndContinue() == true) {
                    self.dispose();
                    System.exit(0);
                }
            }

            @Override
            public void windowOpened(WindowEvent e) {
                try {
                    loadStudentData(new File(Constants.DEFAULT_STARTUP_XML_FILE_PATH));
                    appView.jListIDs.setSelectedIndex(0);
                } catch (Exception ex) {
                    pathToXmlData = Constants.DEFAULT_STARTUP_XML_FILE_PATH;
                    appView.displayStudentDataPath();
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(self, "Cannot locate and load default XML data.\nOriginal error: " + ex.toString());
                }
                appView.updateButtonStates();
            }
        });

        appView.jListIDs.addListSelectionListener((event) -> {
            Object ID = appView.jListIDs.getSelectedValue();
            if (ID != null) {
                StudentInfo info = studentInfoSet.getStudent(ID.toString());
                if (info != null) {
                    appView.showNewStudentPanel(info.getStudentPanel());
                }
                appView.updateButtonStates();
            }
        });

        menuBar.menuItemLoadSet.addActionListener((event) -> {
            if (!suggestSavingAndContinue()) {
                return;
            }

            File selectedXmlFile = FileManipulation.browseXmlDirectory(this, true);
            if (selectedXmlFile != null) {
                try {
                    loadStudentData(selectedXmlFile);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, "Cannot load XML data.\nOriginal error: " + ex.toString());
                    ex.printStackTrace();
                }
            }
        });

        menuBar.menuItemSave.addActionListener((event) -> {
            boolean wasSaved = saveStudentData();
            if (wasSaved) {
                JOptionPane.showMessageDialog(self, "Student data has been saved.");
            }
        });

        menuBar.menuItemSaveAs.addActionListener((event) -> {
            File selectedFile = FileManipulation.browseXmlDirectory(self, false);
            if (selectedFile != null) {
                saveStudentDataToFile(selectedFile.getPath());
            }
        });

        appView.jButtonDeleteStudent.addActionListener((event) -> {
            Object selectedID = appView.jListIDs.getSelectedValue();
            if (selectedID != null) {
                beginDeleteNewStudent(selectedID.toString());
            } else {
                JOptionPane.showMessageDialog(this, "Select a student first.");
            }
        });

        appView.jButtonNewStudent.addActionListener((event) -> {
            beginAddNewStudent();
        });

        menuBar.menuItemAddStudent.addActionListener((event) -> {
            beginAddNewStudent();
        });

        menuBar.menuItemRemoveStudent.addActionListener((event) -> {
            DlgStudentID dlg = new DlgStudentID(self, studentInfoSet.getStudentKeys(), false);
            String IDToRemove = dlg.getSelectedID();
            dlg.dispose();

            if (IDToRemove != null) {
                beginDeleteNewStudent(IDToRemove);
            }
        });

        menuBar.menuItemRainbow.addActionListener((event) -> {
            supplierBorder = (width, height) -> {
                return Reusable.getRainbowGradient(width, height);
            };
            appView.updatePhotoBorder();
        });

        menuBar.menuItemTwoColorCyan.addActionListener((event) -> {
            supplierBorder = (width, height) -> {
                return Reusable.getTwoColorGradientCyan(width, height);
            };
            appView.updatePhotoBorder();
        });

        menuBar.menuItemTwoColorRed.addActionListener((event) -> {
            supplierBorder = (width, height) -> {
                return Reusable.getTwoColorGradientRed(width, height);
            };
            appView.updatePhotoBorder();
        });

        menuBar.menuItemDefault.addActionListener((event) -> {
            supplierBorder = (width, height) -> {
                return Reusable.getDefaultBorder(width, height);
            };
            appView.updatePhotoBorder();
        });

        menuBar.menuItemNewSet.addActionListener((event) -> {
            if (!suggestSavingAndContinue()) {
                return;
            }
            setPathToXmlData(null);
            studentInfoSet.clear();
            studentInfoSet.setDescription(null);
            appView.resetUI();
        });
    }

    public boolean suggestSavingAndContinue() {
        boolean moveOn = false;

        if (studentInfoSet != null) {
            int result = JOptionPane.showConfirmDialog(this, "Any unsaved changes will be lost.\n\nWould you like to save your current student data?\n\n",
                    "Save", JOptionPane.YES_NO_CANCEL_OPTION);

            switch (result) {
                case JOptionPane.YES_OPTION:
                    boolean wasSaved = saveStudentData();
                    moveOn = wasSaved;
                    break;
                case JOptionPane.NO_OPTION:
                    moveOn = true;
                    break;
                default:
                    moveOn = false;
                    break;
            }
        } else {
            moveOn = true;
        }

        return moveOn;
    }

    private void loadStudentData(File xmlFile) throws Exception {
        FileInputStream fis = new FileInputStream(xmlFile);

        DocumentBuilder builder = Reusable.createDocBuilder(this);
        Document document = builder.parse(fis);
        document.getDocumentElement().normalize();
        fis.close();

        boolean wasLoaded = true;
        try {
            studentInfoSet.loadStudentsFromXML(document);
        } catch (Exception ex) {
            wasLoaded = false;
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error while parsing the document: " + ex.toString());
        }

        if (wasLoaded) {
            setPathToXmlData(xmlFile.getPath());
            appView.resetUI();
        }
    }

    private boolean saveStudentData() {
        boolean wasSaved = false;
        if (pathToXmlData == null) {
            File selectedFile = FileManipulation.browseXmlDirectory(self, false);
            if (selectedFile != null) {
                wasSaved = saveStudentDataToFile(selectedFile.getPath());
            }
        } else {
            wasSaved = saveStudentDataToFile(pathToXmlData);
        }
        return wasSaved;
    }

    private boolean saveStudentDataToFile(String path) {
        boolean wasSaved = true;

        studentInfoSet.setDescription(appView.jTextFieldSetDescription.getText());
        try {
            Document doc = studentInfoSet.saveStudentsToXml(this);

            OutputStream fos = Files.newOutputStream(Paths.get(path));
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(fos));

            setPathToXmlData(path);
        } catch (Exception ex) {
            wasSaved = false;
            JOptionPane.showMessageDialog(this, "Failed to save student data: \n\n" + ex);
        }

        return wasSaved;
    }

    private void setPathToXmlData(String path) {
        pathToXmlData = path;
        appView.displayStudentDataPath();
    }

    private void beginAddNewStudent() {
        DlgStudentID dlg = new DlgStudentID(self, studentInfoSet.getStudentKeys(), true);
        String selectedID = dlg.getSelectedID();
        dlg.dispose();

        if (selectedID != null) {
            studentInfoSet.addStudent(new StudentInfo(selectedID));
            appView.updateList();
            appView.jListIDs.setSelectedValue(selectedID, true);
            appView.updateButtonStates();
            appView.displayNotification("A new student with ID " + selectedID + " has been added.");
        }
    }

    private void beginDeleteNewStudent(String studentID) {
        if (studentID != null) {
            studentInfoSet.removeStudent(studentID);

            Object listSelectedID = appView.jListIDs.getSelectedValue();
            appView.resetUI();
            if (listSelectedID != null && !studentID.equalsIgnoreCase(listSelectedID.toString())) {
                //Reselect lost selection due to updateList()
                appView.jListIDs.setSelectedValue(listSelectedID, true);
            }
            appView.displayNotification("Student with ID " + studentID + " has been removed.");
        }
    }

    private class AppView extends JPanel implements Notifiable {

        JSplitPane jSplitPane; //1) Direct Child
        StatusView statusView; //1) Direct Child

        JPanel jPanelStudentList; //2) Left Child of jSplitPane
        JPanel jPanelStudentListTop; //Fix JAVA
        JPanel jPanelStudentListBottom; //Fix JAVA
        JButton jButtonNewStudent; // 3) Top Child of jPanelStudentListTop
        JButton jButtonDeleteStudent; // 3) Top Child of jPanelStudentListTop
        JScrollPane jScrollPaneForIDs; //3) Bottom Child of jPanelStudentListTop
        JListJavaFix jListIDs;

        JLabel jLabelNotificationArea; //4) Child of jPanelStudentListBottom
        JTextArea jTextAreaNotification; //4) Child of jPanelStudentListBottom

        JPanel jPanelStudentView;//5) Right Child of jSplitPane
        JComponent jComponentStudent; //6) Center Child of jPanelStudentView
        JPanel jPanelSetDescr; //6) Top child of jPanelStudentView
        JLabel jLabelSetDescription;
        JTextField jTextFieldSetDescription;

        private Timer timerColorAnimator;
        private TimerTask taskAnimateColor;
        private TimerTask taskAnimateColorReverse;

        public AppView() {
            super(new BorderLayout());

            this.timerColorAnimator = new Timer();

            initLayout();
            loadLayout();
        }

        private void initLayout() {
            statusView = new StatusView();

            jPanelStudentList = new JPanel(new BorderLayout());
            jPanelStudentListTop = new JPanel(new GridBagLayout());

            jPanelStudentListBottom = new JPanel(new BorderLayout());
            jPanelStudentListBottom.setBorder(new EmptyBorder(0, 3, 3, 3));
            jButtonNewStudent = new JButton(Constants.NEW_STUDENT);
            jButtonDeleteStudent = new JButton(Constants.DELETE_STUDENT);

            jListIDs = new JListJavaFix<>(studentInfoSet.getStudentKeys().toArray());
            jListIDs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            jScrollPaneForIDs = new JScrollPane(jListIDs);
            jScrollPaneForIDs.setAlignmentX(Component.CENTER_ALIGNMENT);
            jScrollPaneForIDs.setMinimumSize(new Dimension(100, 50));
            jScrollPaneForIDs.setPreferredSize(jButtonDeleteStudent.getPreferredSize()); //Fixes divider location of -1, which should respect preferred sizes

            jLabelNotificationArea = new JLabel("Notification Area:");
            jLabelNotificationArea.setBorder(new EmptyBorder(5, 3, 0, 3));
            jTextAreaNotification = new JTextArea();

            jTextAreaNotification.setEditable(false);
            jTextAreaNotification.setCursor(null);
            jTextAreaNotification.setFocusable(false);
            jTextAreaNotification.setPreferredSize(new Dimension(120, 80));
            jTextAreaNotification.setMinimumSize(new Dimension(120, 10)); //This prevents jSplitPane from being unable to shrink when window resizes
            jTextAreaNotification.setLineWrap(true);
            jTextAreaNotification.setBackground(UIManager.getColor("Label.background"));
            jTextAreaNotification.setWrapStyleWord(true);
            jTextAreaNotification.setBorder(new CompoundBorder(
                    new StrokeBorder(new BasicStroke(1), Color.BLACK),
                    new EmptyBorder(3, 3, 3, 3)));

            jPanelSetDescr = new JPanel(new BorderLayout());
            jPanelSetDescr.setBorder(new CompoundBorder(new MatteBorder(0, 0, 2, 0, Color.blue), new EmptyBorder(4, 4, 2, 4)));
            jLabelSetDescription = new JLabel(Constants.SET_DESCRIPTION);
            jLabelSetDescription.setAlignmentY(1.0f);
            jTextFieldSetDescription = new JTextField();
            jPanelStudentView = new JPanel(new BorderLayout());
            jPanelStudentView.setMinimumSize(new Dimension(30, 30)); //Controls how jPaneSplit resizes to the right

            jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, jPanelStudentList, jPanelStudentView);
            jSplitPane.setOneTouchExpandable(true);
            jSplitPane.setDividerLocation(-1);
//        jSplitPane.setDividerSize(10);
        }

        private void loadLayout() {
            jPanelStudentListBottom.add(jLabelNotificationArea, BorderLayout.NORTH);
            jPanelStudentListBottom.add(jTextAreaNotification, BorderLayout.CENTER);

            jPanelStudentList.add(jPanelStudentListBottom, BorderLayout.SOUTH);
            jPanelStudentList.add(jPanelStudentListTop, BorderLayout.CENTER);

            int anchor = GridBagConstraints.FIRST_LINE_START;
            int fill = GridBagConstraints.BOTH;
            Insets insets = new Insets(0, 0, 0, 0);
            jPanelStudentListTop.add(jButtonNewStudent, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
            jPanelStudentListTop.add(jButtonDeleteStudent, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
            jPanelStudentListTop.add(jScrollPaneForIDs, new GridBagConstraints(0, 2, 1, 1, 1.0d, 1.0d, anchor, fill, insets, 0, 0));

            jPanelSetDescr.add(jLabelSetDescription, BorderLayout.WEST);
            jPanelSetDescr.add(jTextFieldSetDescription, BorderLayout.CENTER);
            jPanelStudentView.add(jPanelSetDescr, BorderLayout.NORTH);

            this.add(jSplitPane, BorderLayout.CENTER);
            this.add(statusView, BorderLayout.SOUTH);
        }

        @Override
        public void displayNotification(String message) {
            if (taskAnimateColor != null) {
                taskAnimateColor.cancel();
            }
            if (taskAnimateColorReverse != null) {
                taskAnimateColorReverse.cancel();
            }

            jTextAreaNotification.setText(message);
            jTextAreaNotification.setForeground(new Color(0, 0, 0, 0));

            taskAnimateColorReverse = new TimerTask() {
                @Override
                public void run() {
                    Color color = new Color(0, 0, 255, Math.max(jTextAreaNotification.getForeground().getAlpha() - 5, 0));
                    jTextAreaNotification.setForeground(color);
                    jTextAreaNotification.repaint();

                    if (jTextAreaNotification.getForeground().getAlpha() == 0) {
//                        jTextAreaNotification.setText("");
                        taskAnimateColorReverse.cancel();
                        taskAnimateColorReverse = null;
                    }
                }
            };
            taskAnimateColor = new TimerTask() {
                @Override
                public void run() {
                    Color color = new Color(0, 0, 255, Math.min(jTextAreaNotification.getForeground().getAlpha() + 5, 255));
                    jTextAreaNotification.setForeground(color);
                    jTextAreaNotification.repaint();

                    if (jTextAreaNotification.getForeground().getAlpha() == 255) {
                        timerColorAnimator.schedule(taskAnimateColorReverse, 2000, 20);
                        taskAnimateColor.cancel();
                        taskAnimateColor = null;
                    }
                }
            };

            timerColorAnimator.schedule(taskAnimateColor, 0, 20);
        }

        private void resetUI() {
            if (studentInfoSet.getDescription() != null) {
                appView.jTextFieldSetDescription.setText(studentInfoSet.getDescription());
            } else {
                appView.jTextFieldSetDescription.setText("");
            }

            displayStudentDataPath();
            if (jComponentStudent != null) {
                jPanelStudentView.remove(jComponentStudent);
                jComponentStudent = null;
            }
            updateList();
            jPanelStudentView.repaint(); //validate() keeps the drawing unfortunately
            updateButtonStates();
        }

        private void showNewStudentPanel(JComponent studentPanel) {
            if (jComponentStudent != null) {
                jPanelStudentView.remove(jComponentStudent);
            }
            jComponentStudent = studentPanel;

            //gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
            jPanelStudentView.add(jComponentStudent, BorderLayout.CENTER);
            jPanelStudentView.validate();
            updatePhotoBorder(); //Experiment
            if (jComponentStudent != null) {
                ((StudentInfo.StudentPanel) jComponentStudent).setNotifiable(this);
            }
        }

        private void updateList() {
            //Could also use generics: studentInfoSet.getStudentKeys().toArray(new String[0]);
            //But why on Earth would I need to workaround that???
            Object[] ids = studentInfoSet.getStudentKeys().toArray();
            for (int i = 0; i < ids.length; i++) {
                ids[i] = Constants.ID_STRING + ids[i].toString();
            }

            jListIDs.setListData(ids);
        }

        private void displayStudentDataPath() {
            String displayPath = (pathToXmlData != null) ? Paths.get(pathToXmlData).getFileName().toString() : "[null]";
            statusView.getFilePathLabel().setText("Student Data File Name: " + "\"" + displayPath + "\"");
        }

        private void updateButtonStates() {
            if (jListIDs.getSelectedValue() != null) {
                jButtonDeleteStudent.setEnabled(true);
            } else {
                jButtonDeleteStudent.setEnabled(false);
            }
        }

        private void updatePhotoBorder() {
            //Experiment
            if (jComponentStudent != null && supplierBorder != null) {
                JScrollPane photo = ((StudentInfo.StudentPanel) jComponentStudent).getScrollPanePhoto();
                Border border = supplierBorder.apply(photo.getWidth(), photo.getHeight());
                photo.setBorder(border);

//                border = supplierBorder.apply(jPanelSetDescr.getWidth(), jPanelSetDescr.getHeight());
//                jPanelSetDescr.setBorder(new CompoundBorder(border, new EmptyBorder(4, 4, 2, 4)));
            }
        }

        private class JListJavaFix<E> extends JList<E> {

            public JListJavaFix(final E[] listData) {
                super(listData);
            }

            @Override
            public void setSelectedValue(Object anObject, boolean shouldScroll) {
                if (anObject != null) {
                    anObject = Constants.ID_STRING + anObject.toString();
                }
                super.setSelectedValue(anObject, shouldScroll);
            }

            @Override
            public E getSelectedValue() {
                E value = super.getSelectedValue();
                if (value != null) {
                    value = (E) value.toString().replace(Constants.ID_STRING, "");
                }
                return value;
            }

        }
    }
}
