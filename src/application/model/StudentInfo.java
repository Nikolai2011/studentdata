/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

import application.other.Constants;
import application.other.FileManipulation;
import application.other.IconApply;
import application.other.IconMissingPhoto;
import application.other.Notifiable;
import application.other.Reusable;
import application.views.DlgStudentCourses;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class StudentInfo {

    private String firstname;
    private String lastname;
    private String birthdate;
    private String studentID;
    private String pictureFileLocation;
    private char gender;
    private List<String> enrolledPapers;

    private static final String empty = "[empty]";

    public StudentInfo(String studentID) {
        this(studentID, empty, empty, Constants.DEFAULT_BIRTHDAY, Constants.DEFAULT_GENDER);
    }

    public StudentInfo(String studentID, String firstname, String lastname, String birthdate, char gender) {
        this.studentID = (studentID == null) ? "0" : studentID;
        this.firstname = (firstname == null) ? empty : firstname;
        this.lastname = (lastname == null) ? empty : lastname;
        this.birthdate = (birthdate == null) ? Constants.DEFAULT_BIRTHDAY : birthdate;
        this.gender = gender;
        enrolledPapers = new ArrayList<>();

    }

    public void addPaper(String paper) {
        enrolledPapers.add(paper);
    }

    public List<String> getEnrolledPapers() {
        enrolledPapers.sort(null);
        return enrolledPapers;
    }

    public void setPictureFromFileName(String filename) {
        this.pictureFileLocation = filename;
    }

    public String getPictureFileName() {
        return pictureFileLocation;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getStudentID() {
        return studentID;
    }

    public char getGender() {
        return gender;
    }

    public JComponent getStudentPanel() {
        return new StudentPanel(this);
    }

    public static class StudentPanel extends JPanel {

        //TEMP DATA
        StudentInfo studentInfo;
        String imagePath;

        //Experiment
        Notifiable notifiable;

        //GUI
        JPanel fieldsPanel;
        JPanel buttonsPanel;

        JPanel jPanelPhotoFixer; //Fix Java
        ImageIcon imageIconPhoto;
        JLabel jLabelPhoto;
        JScrollPane jScrollPanePhoto;

        //FIELDS PANEL
        JLabel jLabelFName;
        JLabel jLabelLName;
        JLabel jLabelGender;
        JLabel jLabelBirthdate;
//        JPanel jPanelForTextFields; //Fix Java
        JTextField jTextFieldFName;
        JTextField jTextFieldLName;
        JComboBox<Integer> jComboBoxYear;
        JComboBox<Integer> jComboBoxMonth;
        JComboBox<Integer> jComboBoxDay;
        JRadioButton jRadioButtonMale;
        JRadioButton jRadioButtonFemale;

        //BUTTONS PANEL
        JButton buttonCourses;
        JButton buttonChangePicture;
        JButton buttonApply;

        public StudentPanel(StudentInfo studentInfo) {

            super(new GridBagLayout());
            this.studentInfo = studentInfo;

            initLayout();
            attachHandlers();
            loadLayout();
        }

        private void initLayout() {
            initLayoutFieldsPanel();
            loadLayoutFieldsPanel();

            initLayoutButtonsPanel();
            loadLayoutButtonsPanel();

            jLabelPhoto = new JLabel();
            setupPhoto(studentInfo.getPictureFileName());
            if (imageIconPhoto == null) {
                jLabelPhoto.setIcon(new IconMissingPhoto());
            }

            //Yes, this is the only way to center align a photo (JLabel) inside a JScrollPane
            jPanelPhotoFixer = new JPanel(new GridBagLayout());
            jPanelPhotoFixer.add(jLabelPhoto);
            jScrollPanePhoto = new JScrollPane(jPanelPhotoFixer);
        }

        private void setupPhoto(String photoPath) {
            if (photoPath != null) {
                File file = new File(photoPath);
                if (file.exists()) {
                    jLabelPhoto.setIcon(null);
                    imageIconPhoto = new ImageIcon(photoPath);
                    jLabelPhoto.setIcon(imageIconPhoto);
                    jLabelPhoto.setText("");
                    imagePath = photoPath;
                } else {
                    jLabelPhoto.setText(Constants.IMAGE_NOT_FOUND + " Location: \"" + photoPath + "\"");
                }
            } else {
                jLabelPhoto.setText(Constants.MISSING_ICON);
            }
        }

        private void attachHandlers() {
            buttonChangePicture.addActionListener((event) -> {
                File imageFile = FileManipulation.browseImagesDirectory(this, true);
                if (imageFile != null) {
                    setupPhoto(imageFile.getPath());
                }
            });

            buttonApply.addActionListener((event) -> {
                String birthdate = Reusable.parseDate(new Integer[]{
                    Integer.valueOf(jComboBoxDay.getSelectedItem().toString()),
                    Integer.valueOf(jComboBoxMonth.getSelectedItem().toString()),
                    Integer.valueOf(jComboBoxYear.getSelectedItem().toString())});

                studentInfo.birthdate = birthdate;
                studentInfo.firstname = jTextFieldFName.getText();
                studentInfo.lastname = jTextFieldLName.getText();
                studentInfo.gender = (jRadioButtonMale.isSelected()) ? Constants.CHAR_MALE : Constants.CHAR_FEMALE;
                studentInfo.setPictureFromFileName(imagePath);
//                JOptionPane.showMessageDialog(this, "Changes have been applied.");
                if (notifiable != null) {
                    notifiable.displayNotification("Student has been modified. To keep changes save the XML file as well.");
                }
            });

            buttonCourses.addActionListener((event) -> {
                DlgStudentCourses dlg = new DlgStudentCourses((JFrame) SwingUtilities.getWindowAncestor(this),
                        studentInfo.getEnrolledPapers(), studentInfo.getStudentID());
                dlg.dispose();
            });
        }

        private void loadLayout() {
            //Hey Java, why does it have to be like that?
            add(fieldsPanel, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            add(jScrollPanePhoto, new GridBagConstraints(0, 1, 1, 1, 1.0d, 1.0d, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            add(buttonsPanel, new GridBagConstraints(0, 2, 1, 1, 0.0d, 0.0d, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }

        private void initLayoutFieldsPanel() {
            fieldsPanel = new JPanel(new GridBagLayout());

            jLabelFName = new JLabel("First Name");
            jLabelLName = new JLabel("Last Name");
            jLabelGender = new JLabel("Gender");
            jLabelBirthdate = new JLabel("Birth date (day/month/year)");
            jTextFieldFName = new JTextField(studentInfo.getFirstname());
            jTextFieldLName = new JTextField(studentInfo.getLastname());

            int prefHeight = jTextFieldFName.getPreferredSize().height;
            jTextFieldFName.setPreferredSize(new Dimension(100, prefHeight)); //Fix Java
            jTextFieldLName.setPreferredSize(new Dimension(100, prefHeight)); //Fix Java

//            jPanelForTextFields = new JPanel(new GridBagLayout());
//            jPanelForTextFields.setMinimumSize(new Dimension(100, prefHeight));
            Integer[] birthdate = Reusable.parseDate(studentInfo.getBirthdate());
            jComboBoxDay = new JComboBox<Integer>(Reusable.getValidDays());
            jComboBoxDay.setSelectedItem(birthdate[0]);
            jComboBoxMonth = new JComboBox<Integer>(Reusable.getValidMonths());
            jComboBoxMonth.setSelectedItem(birthdate[1]);
            jComboBoxYear = new JComboBox<Integer>(Reusable.getValidYears());
            jComboBoxYear.setSelectedItem(birthdate[2]);

            jRadioButtonMale = new JRadioButton(Constants.MALE);
            jRadioButtonFemale = new JRadioButton(Constants.FEMALE);

            ButtonGroup group = new ButtonGroup();
            group.add(jRadioButtonFemale);
            group.add(jRadioButtonMale);
            jRadioButtonMale.setSelected(studentInfo.getGender() == Constants.CHAR_MALE);
            jRadioButtonFemale.setSelected(!jRadioButtonMale.isSelected());
        }

        private void loadLayoutFieldsPanel() {

            double weight = 0.0d;
            double weightTextFields = 1.0d;

            //top left bottom right
            int insetOutside = 15;
            int insetInsideClose = 3;
            int insetInsideFar = 10;
            Insets insetsTop = new Insets(insetOutside, insetInsideFar, 0, 0);
            Insets insetsTopRight = new Insets(insetOutside, insetInsideFar, 0, insetOutside);
            Insets insetsBottomRight = new Insets(insetInsideClose, insetInsideFar, insetOutside, insetOutside);
            Insets insetsBottom = new Insets(insetInsideClose, insetInsideFar, insetOutside, 0);
            Insets insetsBottomLeft = new Insets(insetInsideClose, insetOutside, insetOutside, 0);
            Insets insetsTopLeft = new Insets(insetOutside, insetOutside, 0, 0);

            int pads = 0;
            int anchorTop = GridBagConstraints.FIRST_LINE_START;
            int anchorBottom = GridBagConstraints.FIRST_LINE_START;
            int topFill = GridBagConstraints.BOTH;
            int bottomFill = GridBagConstraints.BOTH;

            //gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
            fieldsPanel.add(jLabelFName, new GridBagConstraints(0, 0, 1, 1, weight, weight,
                    anchorTop, topFill, insetsTopLeft, pads, pads));
            fieldsPanel.add(jLabelLName, new GridBagConstraints(1, 0, 1, 1, weight, weight,
                    anchorTop, topFill, insetsTop, pads, pads));
            fieldsPanel.add(jLabelGender, new GridBagConstraints(2, 0, 2, 1, weight, weight,
                    anchorTop, topFill, insetsTop, pads, pads));
            fieldsPanel.add(jLabelBirthdate, new GridBagConstraints(4, 0, 3, 1, weight, weight,
                    anchorTop, topFill, insetsTopRight, pads, pads));

//            jPanelForTextFields.add(jTextFieldFName, new GridBagConstraints(0, 0, 1, 1, weightTextFields, weight,
//                    anchorBottom, bottomFill, insetsBottomLeft, pads, pads));
//            jPanelForTextFields.add(jTextFieldLName, new GridBagConstraints(1, 0, 1, 1, weightTextFields, weight,
//                    anchorBottom, bottomFill, insetsBottom, pads, pads));
//            fieldsPanel.add(jPanelForTextFields, new GridBagConstraints(0, 1, 2, 1, weightTextFields, weight,
//                    anchorBottom, bottomFill, new Insets(0, 0, 0, 0), pads, pads));
            fieldsPanel.add(jTextFieldFName, new GridBagConstraints(0, 1, 1, 1, weightTextFields, weight,
                    anchorBottom, bottomFill, insetsBottomLeft, pads, pads));
            fieldsPanel.add(jTextFieldLName, new GridBagConstraints(1, 1, 1, 1, weightTextFields, weight,
                    anchorBottom, bottomFill, insetsBottom, pads, pads));
            fieldsPanel.add(jRadioButtonFemale, new GridBagConstraints(2, 1, 1, 1, weight, weight,
                    anchorBottom, bottomFill, insetsBottom, pads, pads));
            fieldsPanel.add(jRadioButtonMale, new GridBagConstraints(3, 1, 1, 1, weight, weight,
                    anchorBottom, bottomFill, insetsBottom, pads, pads));
            fieldsPanel.add(jComboBoxDay, new GridBagConstraints(4, 1, 1, 1, weight, weight,
                    anchorBottom, bottomFill, insetsBottom, pads, pads));
            fieldsPanel.add(jComboBoxMonth, new GridBagConstraints(5, 1, 1, 1, weight, weight,
                    anchorBottom, bottomFill, insetsBottom, pads, pads));
            fieldsPanel.add(jComboBoxYear, new GridBagConstraints(6, 1, 1, 1, weight, weight,
                    anchorBottom, bottomFill, insetsBottomRight, pads, pads));
        }

        private void initLayoutButtonsPanel() {
            buttonsPanel = new JPanel();
            buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.LINE_AXIS));
            buttonsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

            buttonApply = new JButton(Constants.APPLY);
            buttonApply.setIcon(new IconApply(buttonApply));
            buttonCourses = new JButton(Constants.COURSE_INFO);
            buttonChangePicture = new JButton(Constants.CHANGE_PICTURE);
        }

        private void loadLayoutButtonsPanel() {
            buttonsPanel.add(buttonCourses);
            buttonsPanel.add(buttonChangePicture);
            buttonsPanel.add(Box.createHorizontalGlue());
            buttonsPanel.add(buttonApply);
        }

        public JScrollPane getScrollPanePhoto() {
            //Experiment
            return this.jScrollPanePhoto;
        }

        public void setNotifiable(Notifiable notifiable) {
            this.notifiable = notifiable;
        }
    }

    public static void main(String[] arg) {
        StudentInfo info = new StudentInfo("15897074", "Nikolai", "Kolbenev", "16/06/1996", Constants.CHAR_MALE);
        info.setPictureFromFileName(Constants.DEFAULT_IMAGES_DIRECTORY_PATH + "Simpson.jpg");

        JFrame frame = new JFrame("Fancy Title");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(info.getStudentPanel());
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
