/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

import application.other.Constants;
import application.other.DOMUtilities;
import application.other.Reusable;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class StudentInfoSet implements Iterable<StudentInfo> {

    private Map<String, StudentInfo> studentMap;
    private String description;

    public StudentInfoSet() {
        studentMap = new HashMap<>();
    }

    public void loadStudentsFromXML(Document doc) throws Exception {
        String newDescription = null;

        DOMUtilities domUtil = new DOMUtilities();
        ArrayList<StudentInfo> parsedStudents = new ArrayList<>();

        Node rootXmlNode = doc.getDocumentElement();
        NodeList studentNodes = rootXmlNode.getChildNodes();
        int rootChildrenLength = studentNodes.getLength();

        for (int i = 0; i < rootChildrenLength; i++) {
            StudentInfoHolder infoHolder = new StudentInfoHolder();
            Node studentNode = studentNodes.item(i);
            if (studentNode.getNodeName().equalsIgnoreCase(Constants.EL_DESCRIPTION)) {
                newDescription = domUtil.getTextContent(studentNode);
            } else if (studentNode.getNodeName().equalsIgnoreCase(Constants.EL_STUDENT)) {
                //STUDENT PROCESSING
                infoHolder.studentID = domUtil.getAttributeString(studentNode, Constants.ATTR_STUDENT_ID);
                if (infoHolder.studentID != null) {
                    String gender = domUtil.getAttributeString(studentNode, Constants.ATTR_GENDER);
                    if (gender != null) {
                        infoHolder.gender = gender.charAt(0);
                    } else {
                        infoHolder.gender = Constants.DEFAULT_GENDER;
                    }

                    NodeList studentChildNodes = studentNode.getChildNodes();
                    int studentChildrenLength = studentChildNodes.getLength();

                    for (int j = 0; j < studentChildrenLength; j++) {
                        Node stuChildNode = studentChildNodes.item(j);
                        if (stuChildNode.getNodeName().equalsIgnoreCase(Constants.EL_FIRST_NAME)) {
                            infoHolder.firstname = domUtil.getTextContent(stuChildNode);
                        } else if (stuChildNode.getNodeName().equalsIgnoreCase(Constants.EL_LAST_NAME)) {
                            infoHolder.lastname = domUtil.getTextContent(stuChildNode);
                        } else if (stuChildNode.getNodeName().equalsIgnoreCase(Constants.EL_PICTURE_URL)) {
                            infoHolder.pictureFileLocation = domUtil.getTextContent(stuChildNode);
                        } else if (stuChildNode.getNodeName().equalsIgnoreCase(Constants.EL_BIRTHDAY)) {
                            infoHolder.birthdate = domUtil.getAttributeString(stuChildNode, Constants.ATTR_DAY);
                            infoHolder.birthdate += Constants.DATE_VALUE_DELIMITER;
                            infoHolder.birthdate += domUtil.getAttributeString(stuChildNode, Constants.ATTR_MONTH);
                            infoHolder.birthdate += Constants.DATE_VALUE_DELIMITER;
                            infoHolder.birthdate += domUtil.getAttributeString(stuChildNode, Constants.ATTR_YEAR);
                        } else if (stuChildNode.getNodeName().equalsIgnoreCase(Constants.EL_PAPER)) {
                            infoHolder.enrolledPapers.add(domUtil.getTextContent(stuChildNode));
                        }
                    }

                    StudentInfo info = new StudentInfo(infoHolder.studentID, infoHolder.firstname, infoHolder.lastname, infoHolder.birthdate, infoHolder.gender);
                    info.setPictureFromFileName(infoHolder.pictureFileLocation);
                    info.getEnrolledPapers().addAll(infoHolder.enrolledPapers);
                    parsedStudents.add(info);
                }
            }
        }

        //We should change real data only at the end, otherwise any errors will overwrite what we already have
        studentMap.clear();
        for (StudentInfo student : parsedStudents) {
            studentMap.put(student.getStudentID(), student);
        }
        description = newDescription;
    }

    public Document saveStudentsToXml(Component owner) throws Exception {
        DocumentBuilder builder = Reusable.createDocBuilder(owner);
        Document document = builder.newDocument();

        document.appendChild(document.createElement(Constants.EL_STUDENTS));
        Node rootXMLNode = document.getDocumentElement();

        if (description != null) {
            Node nodeDescr = document.createElement(Constants.EL_DESCRIPTION);
            nodeDescr.setTextContent(description);
            rootXMLNode.appendChild(nodeDescr);
        }

        for (StudentInfo info : this) {
            Element nodeStudent = document.createElement(Constants.EL_STUDENT);
            nodeStudent.setAttribute(Constants.ATTR_STUDENT_ID, info.getStudentID());
            nodeStudent.setAttribute(Constants.ATTR_GENDER, "" + info.getGender());

            if (info.getFirstname() != null) {
                Element nodeFName = document.createElement(Constants.EL_FIRST_NAME);
                nodeFName.setTextContent(info.getFirstname());
                nodeStudent.appendChild(nodeFName);
            }

            if (info.getLastname() != null) {
                Element nodeLName = document.createElement(Constants.EL_LAST_NAME);
                nodeLName.setTextContent(info.getLastname());
                nodeStudent.appendChild(nodeLName);
            }

            if (info.getPictureFileName() != null) {
                Element nodePictureURL = document.createElement(Constants.EL_PICTURE_URL);
                nodePictureURL.setTextContent(info.getPictureFileName());
                nodeStudent.appendChild(nodePictureURL);
            }

            if (info.getBirthdate() != null) {
                Integer[] birthDates = Reusable.parseDate(info.getBirthdate());
                Element nodeBirth = document.createElement(Constants.EL_BIRTHDAY);
                nodeBirth.setAttribute(Constants.ATTR_DAY, birthDates[0].toString());
                nodeBirth.setAttribute(Constants.ATTR_MONTH, birthDates[1].toString());
                nodeBirth.setAttribute(Constants.ATTR_YEAR, birthDates[2].toString());

                nodeStudent.appendChild(nodeBirth);
            }

            if (info.getEnrolledPapers() != null) {
                for (String paper : info.getEnrolledPapers()) {
                    Element nodePaper = document.createElement(Constants.EL_PAPER);
                    nodePaper.setTextContent(paper);
                    nodeStudent.appendChild(nodePaper);
                }
            }

            rootXMLNode.appendChild(nodeStudent);
        }

        return document;
    }

    public void addStudent(StudentInfo student) {
        studentMap.put(student.getStudentID(), student);
    }

    public void removeStudent(String studentID) {
        studentMap.remove(studentID);
    }

    public StudentInfo getStudent(String studentID) {
        return studentMap.get(studentID);
    }

    public void clear() {
        studentMap.clear();
    }

    public Iterator<StudentInfo> iterator() {
        return studentMap.values().iterator();
    }

    public int size() {
        return studentMap.size();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean containsKey(String key) {
        return studentMap.containsKey(key);
    }

    public Set<String> getStudentKeys() {
        return studentMap.keySet();
    }

    private class StudentInfoHolder {

        String firstname;
        String lastname;
        String birthdate;
        String studentID;
        String pictureFileLocation;
        char gender;
        List<String> enrolledPapers = new ArrayList<>();
    }
}
