/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.other;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;
import javax.swing.JButton;

public class IconApply implements Icon {

    int width = 32;
    int height = 32;

    JButton owner;
    Color color;
    BasicStroke stroke;

    public IconApply(JButton owner) {
        this.owner = owner;
        color = new Color(0, 155, 0);
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        try {
            Graphics2D g2d = (Graphics2D) g.create();
            
            color = (owner.getModel().isPressed())
                    ? new Color(0, 0, 255) : new Color(0, 155, 0);
            stroke = (owner.getModel().isPressed())
                    ? new BasicStroke(4) : new BasicStroke(3);
            
            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHints(rh);

                        g2d.setColor(color);
            g2d.setStroke(stroke);
            
            g2d.drawLine(x + width / 4, y + height / 2, x + width / 2, y + height * 3 / 4);
            g2d.drawLine(x + width / 2, y + height * 3 / 4, x + width * 3 / 4, y + height / 4);

            g2d.dispose();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            ex.printStackTrace();
        }
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    @Override
    public int getIconHeight() {
        return height;
    }

}
