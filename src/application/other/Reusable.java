/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.other;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.io.File;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.border.StrokeBorder;
import javax.swing.border.TitledBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class Reusable {

//    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/mm/yyyy");
    public static Integer[] getValidDays() {
        Integer[] days = new Integer[31];
        for (int i = 0; i < days.length; i++) {
            days[i] = i + 1;
        }
        return days;
    }

    public static Integer[] getValidMonths() {
        Integer[] months = new Integer[12];
        for (int i = 0; i < months.length; i++) {
            months[i] = i + 1;
        }
        return months;
    }

    public static Integer[] getValidYears() {
        Integer[] years = new Integer[324];
        for (int i = 0; i < years.length; i++) {
            years[i] = 1900 + i;
        }
        return years;
    }

    /**
     *
     * @param birthdate a date string in the form "dd/mm/yyyy", each of the
     * entries can be converted to integers
     * @return an array of integers: day, month, year
     */
    public static Integer[] parseDate(String birthdate) {
        if (birthdate == null) {
            birthdate = Constants.DEFAULT_BIRTHDAY;
        }
        Integer[] date = new Integer[3];

        try {
            String[] dateTokens = birthdate.split(String.valueOf(Constants.DATE_VALUE_DELIMITER));
            date[0] = Integer.valueOf(dateTokens[0]);
            date[1] = Integer.valueOf(dateTokens[1]);
            date[2] = Integer.valueOf(dateTokens[2]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return date;
    }

    public static String parseDate(Integer[] birthdate) {
        String date = null;
        if (birthdate.length == 3) {
            date = birthdate[0].toString() + Constants.DATE_VALUE_DELIMITER + birthdate[1].toString()
                    + Constants.DATE_VALUE_DELIMITER + birthdate[2].toString();
        }
        return date;
    }

    public static DocumentBuilder createDocBuilder(Component owner) throws Exception {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);

        builderFactory.setAttribute(Constants.JAXP_SCHEMA_LANGUAGE, Constants.W3C_XML_SCHEMA);

        URL schemaURL = Thread.currentThread().getContextClassLoader().getResource(Constants.SCHEMA_VALIDATION_RESOURCE);
        if (schemaURL != null) {
            File schemaFile = new File(schemaURL.getPath());
            if (schemaFile.exists()) {
                builderFactory.setAttribute(Constants.JAXP_SCHEMA_SOURCE, schemaFile.getPath());
                builderFactory.setValidating(true);
            }
        }

        //Another way?
//        SchemaFactory factory = SchemaFactory.newInstance(Constants.W3C_XML_SCHEMA);
//        Schema validationSchema = factory.newSchema(Thread.currentThread().getContextClassLoader().getResource(Constants.SCHEMA_VALIDATION_RESOURCE));
//        builderFactory.setSchema(validationSchema);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();

        builder.setErrorHandler(new ErrorHandler() {
            boolean isShowErrorsOn = true;

            @Override
            public void warning(SAXParseException exception) throws SAXException {
                showError(exception);
            }

            @Override
            public void error(SAXParseException exception) throws SAXException {
                showError(exception);
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                showError(exception);
            }

            private void showError(SAXException ex) {
                if (isShowErrorsOn) {
                    //Experimenting...
                    String error = ex.toString();
                    StringBuilder errorMsg = new StringBuilder();
                    errorMsg.append("");
                    if (ex.toString().length() < 100) {
                        errorMsg.append(ex.toString());
                    } else {
                        for (int i = 0; i < error.length(); i += 100) {
                            errorMsg.append(error.substring(i,
                                    (i + 100) < error.length() ? i + 100 : error.length()));
                            errorMsg.append("\n");
                            if (i > 450) {
                                break;
                            }
                        }
                    }
                    int returnVal = JOptionPane.showOptionDialog(owner, errorMsg + "\n Would you like to receive exceptions that follow?\n\n",
                            "Invalid Format", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);

                    if (returnVal == JOptionPane.NO_OPTION) {
                        isShowErrorsOn = false;
                    }
                }
            }
        });

        return builder;
    }

    public static Border getDefaultBorder(int width, int height) {
        return BorderFactory.createLineBorder(Color.blue, 2);
    }

    public static TitledBorder getTwoColorGradientCyan(int width, int height) {
        return getTwoColorGradient(Color.CYAN, width, height);
    }

    public static TitledBorder getTwoColorGradientRed(int width, int height) {
        return getTwoColorGradient(Color.RED, width, height);
    }

    private static TitledBorder getTwoColorGradient(Color color, int width, int height) {
        GradientPaint twoColorPaint = new GradientPaint(0.0f, 0.0f, Color.BLUE, 50.0f, 50.0f, color, true);
        return new TitledBorder(new StrokeBorder(new BasicStroke(3), twoColorPaint), "", TitledBorder.CENTER, TitledBorder.TOP);
    }

    public static TitledBorder getRainbowGradient(int width, int height) {
        Color[] colors = {Color.CYAN, Color.BLUE, Color.PINK, Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN};
        float[] fractions = new float[7];
        for (int i = 0; i < fractions.length; i++) {
            fractions[i] = i / (float) fractions.length;
        }

        MultipleGradientPaint rainbowPaint = new LinearGradientPaint(0.0f, 0.0f, width, height, fractions, colors);
        return new TitledBorder(new StrokeBorder(new BasicStroke(3), rainbowPaint), "", TitledBorder.CENTER, TitledBorder.TOP);
    }
}
