/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.other;

import java.awt.Component;
import java.io.File;
import java.nio.file.Paths;
import javax.swing.JFileChooser;

public class FileManipulation {

    private static JFileChooser fileChooserXML = new JFileChooser(Constants.DEFAULT_XML_DIRECTORY_PATH);
    private static JFileChooser fileChooserImages = new JFileChooser(Constants.DEFAULT_IMAGES_DIRECTORY_PATH);

    public static void buildPath(String filePath) {
        File file = new File(filePath);
        file.mkdirs();
    }

    public static File browseXmlDirectory(Component owner, boolean showOpenDialog) {
        buildPath(Constants.DEFAULT_XML_DIRECTORY_PATH);
//        fileChooser.setCurrentDirectory(Paths.get(DEFAULT_DATABASE_PATH).getParent().toFile());

        File selectedFile = null;
        int returnVal = (showOpenDialog) ? fileChooserXML.showOpenDialog(owner) : fileChooserXML.showSaveDialog(owner);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooserXML.getSelectedFile();
        }

        return selectedFile;
    }

    public static File browseImagesDirectory(Component owner, boolean showOpenDialog) {
        buildPath(Constants.DEFAULT_IMAGES_DIRECTORY_PATH);

        File selectedFile = null;
        int returnVal = (showOpenDialog) ? fileChooserImages.showOpenDialog(owner) : fileChooserImages.showSaveDialog(owner);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooserImages.getSelectedFile();
        }

        return selectedFile;
    }
}
