/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.other;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;

public class IconMissingPhoto implements Icon {

    private int widthLie = 32;
    private int heightLie = 32;

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        //Better not draw than crash
        try {
            Graphics2D g2d = (Graphics2D) g.create();

            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHints(rh);

            int width = widthLie - 4;
            int height = heightLie - 4;
            x += 2;
            y += 2;

            g2d.setColor(Color.white);
            g2d.fillOval(x, y, width, height);
            g2d.setColor(Color.red);
            g2d.setStroke(new BasicStroke(3));
            g2d.drawOval(x, y, width, height);
            g2d.setStroke(new BasicStroke(4));

            g2d.drawLine(x + width / 4, y + height / 4, x + width * 3 / 4, y + height * 3 / 4);
            g2d.drawLine(x + width / 4, y + height * 3 / 4, x + width * 3 / 4, y + height / 4);

            g2d.dispose();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            ex.printStackTrace();
        }
    }

    @Override
    public int getIconWidth() {
        return widthLie;
    }

    @Override
    public int getIconHeight() {
        return heightLie;
    }

}
