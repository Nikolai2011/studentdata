/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.other;

import java.io.File;
import javax.xml.XMLConstants;

public class Constants {

    public static final String DEFAULT_XML_DIRECTORY_PATH = "studentdata" + File.separator;
    public static final String DEFAULT_STARTUP_XML_FILE_PATH = DEFAULT_XML_DIRECTORY_PATH + "tempcachedata.xml";
    public static final String DEFAULT_IMAGES_DIRECTORY_PATH = "images" + File.separator;

    public static final String JAXP_SCHEMA_SOURCE
            = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    public static final String JAXP_SCHEMA_LANGUAGE
            = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    public static final String W3C_XML_SCHEMA = XMLConstants.W3C_XML_SCHEMA_NS_URI;

    public static final String SCHEMA_VALIDATION_RESOURCE = "data/students.xsd";

    public static final String ID_STRING = "ID "; //This will be replaced with empty string in custom JList class
    public static final String NEW_STUDENT = "New Student";
    public static final String DELETE_STUDENT = "Delete Student";
    public static final String SET_DESCRIPTION = "Set Description:   ";
    public static final String APPLY = "Apply";
    public static final String COURSE_INFO = "View Course Info";
    public static final String CHANGE_PICTURE = "Change Picture";
    public static final String MISSING_ICON = "Image path is missing";
    public static final String IMAGE_NOT_FOUND = "[Image file not found]";
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final char CHAR_MALE = 'M';
    public static final char CHAR_FEMALE = 'F';
    public static final char DEFAULT_GENDER = CHAR_MALE;
    public static final String DEFAULT_BIRTHDAY = "14/05/2017"; //Today
    public static final String DATE_VALUE_DELIMITER = "/";
//    public static final String PATH_TO_ASSETS = "src/";

    //XML NAMES
    public static final String EL_STUDENTS = "students";
    public static final String EL_STUDENT = "student";
    public static final String EL_DESCRIPTION = "description";
    public static final String EL_FIRST_NAME = "firstname";
    public static final String EL_LAST_NAME = "lastname";
    public static final String EL_PICTURE_URL = "pictureURL";
    public static final String EL_BIRTHDAY = "birthday";
    public static final String ATTR_DAY = "day";
    public static final String ATTR_MONTH = "month";
    public static final String ATTR_YEAR = "year";

    public static final String EL_PAPER = "paper";
    public static final String ATTR_STUDENT_ID = "studentID";
    public static final String ATTR_GENDER = "gender";

}
